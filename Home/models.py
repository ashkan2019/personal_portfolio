from django.db import models

class home_page(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)

class about_me(models.Model):
    description_1 = models.CharField(max_length=100)
    description_2 = models.CharField(max_length=100)
    description_3 = models.CharField(max_length=100)
    description_4 = models.CharField(max_length=100)
    description_5 = models.CharField(max_length=100)
    description_6 = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=100)
    resume = models.FileField(default="")

class my_resume(models.Model):
    description_7 = models.CharField(max_length=100)
    first_ability_name = models.CharField(max_length=40,default="") 
    first_ability_percentage = models.IntegerField(default=1)
    second_ability_name = models.CharField(max_length=40,default="") 
    second_ability_percentage = models.IntegerField(default=1)
    third_ability_name = models.CharField(max_length=40,default="") 
    third_ability_percentage = models.IntegerField(default=1)
    forth_ability_name = models.CharField(max_length=40,default="") 
    forth_ability_percentage = models.IntegerField(default=1)
    



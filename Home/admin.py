from django.contrib import admin
from .models import home_page, about_me, my_resume

admin.site.register(home_page)
admin.site.register(about_me)
admin.site.register(my_resume)

